Problem with purchased capture card (ms2130).
More details in the first video.

Source: Amazon Fire TV Stick 4K Max
Splitter: Ezcoo 4k
Capture card, (connected via HDMI)
Receiver: Notebook

As you can see, the source is connected to the “input” input, the lamp is constantly on, the signal is stable.
The capture card is connected to the "out 2" output, the light is flashing.
What does unstable signal mean. It is not normal.

More on the second video.

In the first video, the capture card was connected via an intermediate element (splitter, HDMI cable).
In the second video, the capture card was connected directly to the signal source (to exclude the influence of other devices).

What exactly is the problem. The capture card does not capture information correctly, which causes the colors to be very distorted, which should not be.
This is clearly seen further in the video in the OBS (Open Broadcaster Software) program window.

I have used other old capture cards before (rullz ms2109, ezcap 269) with the same set of hardware for my needs.
And there were no such problems.
